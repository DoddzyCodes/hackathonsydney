﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="new Building", menuName = "Building/new Building", order = 1)]
public class BuildingScriptableObject : ScriptableObject {

   public int m_costToBuild;
   public int m_powerProvided;
   
   public float m_carbonEmissions;

}
