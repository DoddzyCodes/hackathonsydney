﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Civilization : MonoBehaviour {

    int m_currency;
    int m_currPower;
    public int population;

    float populationTimer;

    public List<Building> ownedBuildings;



	// Use this for initialization
	void Start ()
    {
		
	}
    public void OnGameStart()
    {
        population = 100;
    }
	
	// Update is called once per frame
	void Update ()
    {
        PopulationIncrease();
	}

    int GetCivilizationCurrency()
    {
        return m_currency;
    }

    int GetCivilizationPower()
    {
        return m_currPower;
    }

    void PopulationIncrease()
    {
        populationTimer = populationTimer + Time.deltaTime;

        if (populationTimer >= 5.0)
        {
            population = (int)(population * 1.3);
            populationTimer = 0;
        } 
    }

    bool CheckCurrency(int currencyRequired)
    {
        if (m_currency < currencyRequired)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void BuildBuilding(Building desiredBuilding)
    {
        if (CheckCurrency (desiredBuilding.GetCost()) == true)
        {
            ownedBuildings.Add(desiredBuilding);
            m_currency = m_currency - desiredBuilding.GetCost();
            m_currPower = m_currPower + desiredBuilding.GetPower();
        }
    }

    void DestroyBuilding(Building buildingToDestroy)
    {
        m_currPower = m_currPower - buildingToDestroy.GetPower();
        ownedBuildings.Remove(buildingToDestroy);
    }
}
