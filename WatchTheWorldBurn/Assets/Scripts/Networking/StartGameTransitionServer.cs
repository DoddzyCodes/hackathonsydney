﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class StartGameTransitionServer : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
        // NetworkManager.singleton.ser
        //DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		    if(Input.GetKeyDown(KeyCode.R))
            {
            OnGameStart();
        }
	}

    public void OnGameStart()
    {
        if(NetworkManager.singleton.isActiveAndEnabled /* && _manager.numPlayers > 2 */)
        {
           // GetComponent<NetworkManagerHUD>().showGUI = false;

            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach(GameObject player in players)
            {
                StartGameTransitionClient transitionScript = player.GetComponent<StartGameTransitionClient>();
                transitionScript.RpcStartGame();
            }


            SceneManager.LoadScene("MainServer");
        }
    }
}
