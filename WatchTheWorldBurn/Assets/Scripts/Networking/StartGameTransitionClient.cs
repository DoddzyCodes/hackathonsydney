﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class StartGameTransitionClient : NetworkBehaviour {

	[ClientRpc]
    public void RpcStartGame()
    {
        SceneManager.LoadScene("MainClient");
    }
}
