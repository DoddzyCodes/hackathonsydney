﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSimulator : MonoBehaviour {
    public float temperature;
    public float carbonLevel;
    public float seaLevel;
    public float year;

    private float maxTemperature;
    private float minTemperature;

    private float maxCarbon;
    private float minCarbon;

    private float maxSeaLevel;
    private float minSeaLevel;

    public float overallDanger;

    private float tempRisk;
    private float carbonRisk;
    private float seaRisk;


    public float test1;
    public float test2;

	// Use this for initialization
	void Start ()
    {
        OnGameStart();
	}
	public void OnGameStart ()
    {
        year = 1800;
        temperature = 20;
        carbonLevel = 20;
        seaLevel = 20;


        minTemperature = 0;
        maxTemperature = 100;

        minCarbon = 0;
        maxCarbon = 100;

        minSeaLevel = 0;
        maxSeaLevel = 100;
    }
	// Update is called once per frame
	void Update ()
    {
        CalculateOverallDanger();
        year = year + Time.deltaTime;
	}


    void CalculateOverallDanger()
    {


        tempRisk = CalculatePercentageBounding(minTemperature, maxTemperature, temperature);
        if (tempRisk >= 1)
        {
            Debug.Log("You burnt the planet");
        }
        carbonRisk = CalculatePercentageBounding(minCarbon, maxCarbon, carbonLevel);
        if (carbonRisk >= 1)
        {
            Debug.Log("You suffocated the planet");
        }
        seaRisk = CalculatePercentageBounding(minSeaLevel, maxSeaLevel, seaLevel);
        if (seaRisk >= 1)
        {
            Debug.Log("You drowned the planet");
        }
        overallDanger = (tempRisk + carbonRisk + seaRisk) / 3;
    }

    float CalculatePercentageBounding(float min, float max, float actual)
    {
        float top;
        float bottom;
        float percentage;
        top = actual - min;
        bottom = max - min;
        percentage = top / bottom;
        return percentage;
    }

    float CalculatePercentage(float max, float actual)
    {
        float percentage = (actual / max) * 100;
        return percentage;
    }
}
