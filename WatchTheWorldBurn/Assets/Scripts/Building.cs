﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Building {

    public BuildingScriptableObject BuildingData;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int GetCost()
    {
        return BuildingData.m_costToBuild;
    }

    public void SetCost(int a_costToBuild)
    {
        BuildingData.m_costToBuild = a_costToBuild;
    }

    public int GetPower()
    {
        return BuildingData.m_powerProvided;
    }

    public void SetPower(int a_powerProvided)
    {
        BuildingData.m_powerProvided = a_powerProvided;
    }
}
